## Vault  

 * Encrypted key/value store
 * Dynamic secrets 
 * Leasing, renewal and revocation  
 * Auditing   
 * Rich ACL    

Note:
## Vault 
*  Modern security management software from Hashicorp
* Written in Golang  
* In simple terms,  Vault is a secret key value store accessible through HTTP

## Features
* Dynamic secrets do not exist until they are read.
* Dynamic secrets are generated when they are accessed.
* HTTP API to store and get secrets
* Fine grained access control. 
* Every request/response is logged and can be audited
* For your microservices, you can achieve boundary of context for your secrets using Vault.


---

######  Vault Setup 

![](images/vault-share.png) <!-- .element height="500"-->

```bash
    vault init 
    vault operator unseal << key1, key2,key3 
```

Note:
* When you run vault for first time it is sealed.  
* Vault uses shared shamir secret algorithm for unsealing
* When you initialise vault it gives you 5 unseal keys
*  To unseal vault you need 3 of them.

---

###### Why Vault ?

![](https://raw.githubusercontent.com/cncf/landscape/master/landscape/CloudNativeLandscape_latest.png)<!-- .element height="500"-->

Note:
*   Getting Security right is hard 
* Getting security right in today cloud native is even harder .
* We use many cloud providers AWS/GCP/Azure ..
* Access keys, API keys , 
* Centralised logs 
* Backups in S3.


@@@

###### Why Vault ?
![](images/commit.jpg)<!-- .element height="500"-->

Note:
*   You dont want your name to be on  Hacker news !!


@@@

###### Why Vault ?
![](images/jenkins.png)


Note:
*  Jenkins mask password plugin!

---

##   Architecture 
![](images/vault-architecture.png)<!-- .element height="500"-->


Note:
* Content in barrier is encrypted using 256-bit AES in GCM mode
* Only the storage backend and the HTTP API are outside, all other components are inside the barrier.
* The storage backend is untrusted and is used to durably store encrypted data.


@@@

###### Secrets.yaml

```yaml
  aws:
    access_key_id:
    secret_access_key:
  db:
    mysql:
      username:
      password:

```


Note:
* A typical secret config you want for your microservice or lambda might look like this
* Lets go through how you secure them


@@@

###### Reading and Writing Secrets 
    

```bash
 #Write Secret       
 vault write local/services/svc-demo/mysql.password \
 data=secret
 
 #Read Secret        
 vault read local/services/svc-demo/mysql.password
 
 # Get Secret value as json 
 vault read -format=json  \
 local/services/svc-demo/mysql.password |jq -r .data.data
 #Output:   
 secret
```


Note:
* Storing and reading secrets from vault can be as simple as this.
* You can get your secret from vault back with one line of bash.

* Let us look how we do in GO code

@@@

###### Reading  Secrets in Go

 * [demo/read-secret/main.go](https://gitlab.com/mohan82/meetup/blob/master/demo/read-secret/main.go "Print Secret from Vault")

###### WARNING:
        * Don't use in Prod !!

Note:
###  Main.go
 * Get secret from local/services/svc-demo/mysql.password and print it out


@@@

###### Populating Secrets in Yaml Config

 * [demo/connect-db/main.go](https://gitlab.com/mohan82/meetup/blob/master/demo/connect-db/main.go "Connects to db from getting password from vault")
 
 
Note:
## Main.go
  * Load vault config from data/vault.yaml
  * Load app config from data/config.yaml
  * Get secrets from vault
  * Override secrets from vault in app config
  * Connect to db using the config

---


## Components 
    
* Auth Methods
    - Approle, AWS,Kubernetes,Github..
* Storage Backend 
    - Dynamo DB, S3, Cockroach DB,Postgres,SQL..
* Secret Engines 
   - Key Value ,TOTP, Transit, AWS ,Azure GCP..    
* Audit devices
    - File,Syslog, Socket(TCP/UDP)     



Note:
* To have vault server running in your production environment, you have to have one of above implementations
configured in prod

---


###### Everything is a Path in Vault
 
```bash
 #Mount KV Secret engine to local 

vault secrets enable -path=local/service-auth kv

#Enable Auth Type Kube
vault auth  enable -path="kube/local/"  kubernetes

#Enable Transit secret engine
 vault secrets enable -path="transit/local" transit
 
 
```

Note:
    *  In vault you mount secret engines and auth methods to a path similar to your unix path.
    * Your engine is accessible from the path.
    * Access control by path.
     

---


## Auth Methods
![](images/vault-auth.png)<!-- .element height="500"-->


Note:
    * To access the contents from HTTP User/Machine has to authenticate first 
    * User authentication for writing and reading secrets from vault using Github, Username/Password,Google etc..
    * Machine authentication using EC2 Metadata/Kubernetes service account token
    * After successful authentication retrieve secret from vault
     

---

## Encryption as a Service 

![](images/vault-transit.png)<!-- .element height="500"-->


Note:
* One of our requirement in our venture was to encrypt documents with different keys for each user
* Generate a data key pair (plain text,cipher text) using Transit Engine
* Encrypt the document using the plain text key  and throw away the plian text key
* Store the Document and cipher text key in vault 
* To Decrypt  get the plain text key from vault by passing cipher text key 

---

##  Vault HA  
![](images/vault-aws-architecture.png)<!-- .element height="500"-->

Note:
* To elect a leader Vault uses consensus protocol similar to RAFT
* You have to run 3 or 5 .
* All standby modes will redirect request to active leader
*  Dynamodb is used for `locking` leader _lock

---

##  Questions ??