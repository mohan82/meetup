#### Tools Required for demo

* GO (1.10) For mac from Homebrew (brew install go)
* Docker and Docker compose - For Mac (https://docs.docker.com/docker-for-mac/install/ )

* Go Dep https://github.com/golang/dep  For Mac from Homebrew ( brew install dep)

* Vault CLI  (brew install vault)

* GNU Make https://www.gnu.org/software/make/ ( 3.8.1)

## Git clone

* Ensure $GOPATH is configured

* cd to $GOPATH and clone this repo.

####  Server 
To run server, make sure docker is up and running 
* Run following commands from terminal 

```bash
   cd server && make rund  
   #Note wait for server to startup and run
   make db
```
* To stop  server 

```bash
    cd server && make stop
```  
* To remove server docker containers

```bash
 cd server && make rm
```     
 
## Vault Go Examples  
####  Read Secret

To build and run demo projects,run server first
 
 * Run following commands from terminal `cd` to demo/read-secret and run 
    
  ```bash
     make dep
     make build
     make run 
  ```    
####  Connect DB

To build and run demo projects,run server first
 
 * Run following commands from terminal `cd` to demo/connect-db and run 
    
  ```bash
     make dep
     make build
     make run 
  ```      
  