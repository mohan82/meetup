package main

import (
	"fmt"
	vaultapi "github.com/hashicorp/vault/api"
	"os"
)


// NewVaultClient Default vault client expects the environment variable `VAULT_TOKEN` to be present
func NewVaultClient(address string) (*vaultapi.Client, error) {
	client, err := vaultapi.NewClient(&vaultapi.Config{Address: address})
	if err != nil {
		return nil, err
	}
	return client, nil
}

func vaildate() {
	if os.Getenv("VAULT_ADDR") == "" {
		panic("Cannot find VAULT_ADDR  env variable. Please configure.")

	}
	if os.Getenv("VAULT_TOKEN") == "" {
		panic("Cannot find VAULT_TOKEN  env variable. Please configure.")

	}
}

const dbSecretPath = "local/services/svc-demo/mysql.password"

func main() {
	client, err := NewVaultClient(os.Getenv("VAULT_ADDR"))
	if err != nil {
		panic(fmt.Sprintf("cannot make vault client because :%v", err))
	}
	sec, err := client.Logical().Read(dbSecretPath)
	if err != nil {
		panic(fmt.Sprintf("cannot read password for value at :%s because :%v", dbSecretPath, err))

	}
	if password,ok := sec.Data["data"].(string);ok {
		fmt.Printf("Password is :%s",password)
	}
}
