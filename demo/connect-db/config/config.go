package config

import (
	"fmt"
	"github.com/spf13/viper"
)

type Config struct {
	MySQL struct {
		Username string
		Password string
		Host string
		Port string
		DB string
	}
}

type Secret struct {
	VaultConfig VaultConfig `mapstructure:"vault"`
}
type VaultConfig struct {
	Prefix          string `mapstructure:"prefix"`
	DefaultValueKey string `mapstructure:"default_value_key"`
}

type ViperInterceptor func(*viper.Viper)

func OverrideInterceptor(env string, kv map[string]string) ViperInterceptor {
	return func(vip *viper.Viper) {
		for key, value := range kv {
			vip.Set(fmt.Sprintf("%s.%s", env, key), value)
		}
	}
}

type ViperConfig struct {
	FilePath    string
	FileName    string
	Environment string
}

func NewViper(viperConfig *ViperConfig, ) (*viper.Viper, error) {
	vip := viper.New()
	vip.AddConfigPath(viperConfig.FilePath)
	vip.SetConfigName(viperConfig.FileName)
	vip.SetEnvPrefix(viperConfig.Environment)
	vip.AutomaticEnv()
	err := vip.ReadInConfig()
	if err != nil {
		return nil, fmt.Errorf("cannot load yaml file because :%v", err)
	}
	return vip, nil
}

func LoadVaultConfig(viperConfig *ViperConfig) (*Secret, error) {
	vip, err := NewViper(viperConfig)
	if err != nil {
		return nil, err
	}
	configMap := map[string]*Secret{}
	if err != nil {
		return nil, fmt.Errorf("cannot unmarshal config because :%v ", err)
	}
	err = vip.Unmarshal(&configMap)
	if configMap[viperConfig.Environment] == nil {
		return nil, fmt.Errorf("cannot find config for environment :%s", viperConfig.Environment)
	}
	return configMap[viperConfig.Environment], nil
}

func LoadAppConfig(viperConfig *ViperConfig, interceptors ... ViperInterceptor) (*Config, error) {
	vip, err := NewViper(viperConfig)
	if err != nil {
		return nil, err
	}
	configMap := map[string]*Config{}
	if err != nil {
		return nil, fmt.Errorf("cannot unmarshal config because :%v ", err)
	}
	// go through the interceptors and run
	for _, interceptor := range interceptors {
		interceptor(vip)
	}
	err = vip.Unmarshal(&configMap)
	if configMap[viperConfig.Environment] == nil {
		return nil, fmt.Errorf("cannot find config for environment :%s", viperConfig.Environment)
	}
	return configMap[viperConfig.Environment], nil
}
