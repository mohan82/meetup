package main

import (
	_ "github.com/spf13/viper"
	_ "github.com/go-sql-driver/mysql"
	"os"
	"gitlab.com/mohan82/meetup/demo/connect-db/config"
	"fmt"
	"gitlab.com/mohan82/meetup/demo/connect-db/secret"
	"database/sql"
)

func VaildateEnvVariables() {
	if os.Getenv("ENVIRONMENT") == "" {
		panic("Cannot find ENVIRONMENT env variable. Please configure.")
	}
	if os.Getenv("VAULT_ADDR") == "" {
		panic("Cannot find VAULT_ADDR  env variable. Please configure.")

	}
	if os.Getenv("VAULT_TOKEN") == "" {
		panic("Cannot find VAULT_TOKEN  env variable. Please configure.")

	}
}

func LoadConfig() *config.Config {
	conf, err := config.LoadAppConfig(&config.ViperConfig{
		FilePath:    "data/",
		FileName:    "config",
		Environment: os.Getenv("ENVIRONMENT"),
	}, config.OverrideInterceptor(os.Getenv("ENVIRONMENT"), GetSecretsFromVault()))
	if err != nil {
		panic(fmt.Sprintf("cannot load config because :%v", err))
	}
	return conf
}
func GetSecretsFromVault() (map[string]string) {
	conf, err := config.LoadVaultConfig(&config.ViperConfig{
		FilePath:    "data/",
		FileName:    "secret",
		Environment: os.Getenv("ENVIRONMENT"),
	})
	if err != nil {
		panic(fmt.Sprintf("cannot load vault config because :%v", err))
	}
	v, err := secret.NewVaultClient(os.Getenv("VAULT_ADDR"), &conf.VaultConfig)
	if err != nil {
		panic(fmt.Sprintf("cannot make vault client because :%v", err))
	}
	return v.GetSecrets()
}

func MakeDBConn(config *config.Config) {
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", config.MySQL.Username, config.MySQL.Password, config.MySQL.Host,
		config.MySQL.Port, config.MySQL.DB))

	if err != nil {
		panic(fmt.Sprintf("cannot connect to db because :%v", err))
	}
	err = db.Ping()
	if err != nil {
		panic(fmt.Sprintf("cannot ping  db because :%v", err))
	}
	fmt.Println("successfully pinged db")
}

func main() {
	VaildateEnvVariables()
	MakeDBConn(LoadConfig())


}
