package secret

import (
	vaultapi "github.com/hashicorp/vault/api"
	"gitlab.com/mohan82/meetup/demo/connect-db/config"
)



// VaultClient client for vault
type VaultClient struct {
	client *vaultapi.Client
	config *config.VaultConfig
}

// NewVaultClient Default vault client expects the environment variable `VAULT_TOKEN` to be present
func NewVaultClient(address string, config *config.VaultConfig) (*VaultClient, error) {
	client, err := vaultapi.NewClient(&vaultapi.Config{Address: address})
	if err != nil {
		return nil, err
	}
	return &VaultClient{
		client: client,
		config: config,
	}, nil
}

// GetSecrets based on vault config
func (v *VaultClient) GetSecrets() map[string]string {
	return v.secrets(v.config.Prefix)
}

// GetSecretsFromPath  list all secrets from path and return the key value pair
func (v *VaultClient) GetSecretsFromPath(path string) map[string]string {
	return v.secrets(v.config.Prefix + "/" + path)
}

func (v *VaultClient) secrets(prefix string) map[string]string {
	dataSecret, err := v.client.Logical().List(prefix)
	if err != nil {
		return nil
	}
	if dataSecret == nil {
		return make(map[string]string)
	}
	keys := v.getKeys(dataSecret)
	if keys == nil {
		return make(map[string]string)
	}
	kvMap := make(map[string]string, len(keys))
	for _, key := range keys {
		if k, ok := key.(string); ok {
			path := prefix + "/" + k
			value := v.ReadValue(path, v.config.DefaultValueKey)
			if value != "" {
				kvMap[k] = value
			}
		}
	}
	return kvMap

}

func (v *VaultClient) getKeys(secret *vaultapi.Secret) []interface{} {
	keys := secret.Data["keys"]
	if keys == nil {
		return nil
	}
	k, ok := keys.([]interface{})
	if !ok {
		return nil
	}
	return k
}

// Read reads the given path and returns the json object as
// map[string] interface{}
func (v *VaultClient) Read(path string) map[string]interface{} {
	dataSecret, err := v.client.Logical().Read(path)
	if err != nil {
		return nil
	}
	if dataSecret == nil {
		return make(map[string]interface{})
	}
	return dataSecret.Data
}

// WriteObject writes to the given path and returns  data back error on failure
func (v *VaultClient) WriteObject(path string, obj map[string]interface{}) (*vaultapi.Secret, error) {
	secret, err := v.client.Logical().Write(path, obj)
	if err != nil {
		return nil, err
	}
	return secret, nil
}

// Write writes to the given path and returns error on failure
func (v *VaultClient) Write(path string, data string) error {
	_, err := v.client.Logical().Write(path, map[string]interface{}{
		"data": data,
	})
	if err != nil {
		return err
	}
	return nil
}

// List list the given path and returns all  the data as json object as
// map[string] interface{}
func (v *VaultClient) List() map[string]interface{} {
	dataSecret, err := v.client.Logical().List(v.config.Prefix)
	if err != nil {
		return nil
	}
	return dataSecret.Data
}

// ReadValue  returns the value for given path and key
func (v *VaultClient) ReadValue(path string, key string) string {
	dataSecret, err := v.client.Logical().Read(path)
	if err != nil {
		return ""
	}
	if dataSecret != nil && dataSecret.Data[key] != nil {
		if v, ok := dataSecret.Data[key].(string); ok {
			return v
		}
	}
	return ""
}
