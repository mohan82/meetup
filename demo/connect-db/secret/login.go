package secret

import (
	"io/ioutil"
	"strings"
	"fmt"
	"net/http"
	vaultapi "github.com/hashicorp/vault/api"
	"path"
)

// TokenRetriever  retrieves token
type TokenRetriever interface {
	Retrieve() string
}

// AuthType types of auth are kubernetes, panics otherwise
type AuthType string

// Types of auth methods supported
const (
	AuthTypeKubernetes = AuthType("kubernetes")
	AuthTypeNone       = AuthType("none")
)

// VaultConfig config for vault
type VaultConfig struct {
	Prefix string `mapstructure:"prefix"`
	URL    string `mapstructure:"url"`
	Auth struct {
		Type           AuthType `mapstructure:"type"`
		Path           string   `mapstructure:"path"`
		Role           string   `mapstructure:"role"`
		TokenMountPath string   `mapstructure:"token_mount_path"`
	}
	DefaultValueKey string `mapstructure:"default_value_key"`
}

// VaultKubeTokenRetriever retrieves svc account for
// vault from the kube pod mounted path
type VaultKubeTokenRetriever struct {
	Config *VaultConfig
}

// NewVaultKubeTokenRetriever returns VaultKubeTokenRetriever based
// on vault config
func NewVaultKubeTokenRetriever(config *VaultConfig) *VaultKubeTokenRetriever {
	return &VaultKubeTokenRetriever{
		Config: config,
	}
}

// Retrieve retrieves kube token and returns based on valid
// config panics if cannot retrieve token from mount path
func (kr *VaultKubeTokenRetriever) Retrieve() string {
	if kr.Config.Auth.Type != AuthTypeKubernetes {
		return ""

	}
	token, err := kr.getToken()
	if err != nil {
		panic(fmt.Sprintf("Cannot retrieve token because: %s  ", err))

	}
	return token
}

// getToken  gets token from mount path
func (kr *VaultKubeTokenRetriever) getToken() (string, error) {
	bytes, err := ioutil.ReadFile(kr.Config.Auth.TokenMountPath)
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(string(bytes)), nil

}

// NilTokenRetriever nil token retriever
type NilTokenRetriever struct {
}

// Retrieve return ""
func (n *NilTokenRetriever) Retrieve() string {
	return ""
}

func loginToVault(client *vaultapi.Client, config *VaultConfig, retriever TokenRetriever) (*vaultapi.Secret, error) {
	if _, ok := retriever.(*NilTokenRetriever); ok {
		return nil, nil
	}
	s, err := client.Logical().Write(path.Join(config.Auth.Path, "login"), map[string]interface{}{
		"jwt":  retriever.Retrieve(),
		"role": config.Auth.Role,
	})
	if err != nil {
		return nil, err
	}
	client.SetToken(s.Auth.ClientToken)
	return s, err
}

/// Vault Adapter
type vaultReadFn func(path string) (*vaultapi.Secret, error)
type vaultWriteFn func(path string, data map[string]interface{}) (*vaultapi.Secret, error)
type vaultListFn func(path string) (*vaultapi.Secret, error)

func (v *VaultClient) vaultRead(readFn vaultReadFn) vaultReadFn {
	return func(path string) (*vaultapi.Secret, error) {
		s, e := readFn(path)
		if isPermissionDeniedError(e) {
			loginToVault(v.client, nil, &NilTokenRetriever{})
			return readFn(path)
		}
		return s, e
	}
}

func (v *VaultClient) vaultWrite(writeFn vaultWriteFn) vaultWriteFn {
	return func(path string, data map[string]interface{}) (*vaultapi.Secret, error) {
		s, e := writeFn(path, data)
		if isPermissionDeniedError(e) {
			loginToVault(v.client, nil, &NilTokenRetriever{})
			return writeFn(path, data)
		}
		return s, e
	}
}

func (v *VaultClient) vaultList(listFn vaultListFn) vaultListFn {
	return func(path string) (*vaultapi.Secret, error) {
		s, e := listFn(path)
		if isPermissionDeniedError(e) {
			loginToVault(v.client, nil, &NilTokenRetriever{})
			return listFn(path)
		}
		return s, e
	}
}

// Vault client api sends http status code as raw string
// right now the only way to match is check for status code 403 in error
// https://github.com/hashicorp/vault/issues/2140
func isPermissionDeniedError(err error) bool {
	if err == nil || err.Error() == "" {
		return false
	}
	if strings.ContainsAny(err.Error(), fmt.Sprintf("Code:%d", http.StatusForbidden)) {
		return true
	}
	return false
}
