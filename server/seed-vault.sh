#!/usr/bin/env bash

export ENVIRONMENT=local



function main() {
    vault secrets enable -path=local kv
    vault secrets enable -path=dev kv
    vault write dev/services/svc-demo/mysql.password  data=magic
    vault write local/services/svc-demo/mysql.password  data=magic
    local secret=`vault read local/services/svc-demo/mysql.password`
    echo "Secret :$secret"
}

main
